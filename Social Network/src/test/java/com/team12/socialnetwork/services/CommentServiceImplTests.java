package com.team12.socialnetwork.services;

import com.team12.socialnetwork.entities.DTOmodels.CommentDTO;
import com.team12.socialnetwork.entities.DTOmodels.CommentEditDTO;
import com.team12.socialnetwork.entities.Comment;
import com.team12.socialnetwork.entities.Post;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.repositories.base.CommentsRepository;
import com.team12.socialnetwork.repositories.base.PostsRepository;
import com.team12.socialnetwork.services.base.PostsService;
import com.team12.socialnetwork.services.base.UsersService;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;
import java.util.Collections;
import java.util.List;

import static com.team12.socialnetwork.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)
public class CommentServiceImplTests {

    @Mock
    private CommentsRepository repository;

    @Mock
    private PostsService postsService;

    @Mock
    private UsersService usersService;

    @Mock
    private Principal principal;

    @Mock
    private PostsRepository postsRepository;

    @InjectMocks
    private CommentsServiceImpl commentsService;

    @Test
    public void createShould_CallRepository() {
        // Arrange
        UserInfo userInfo = createUser();
        CommentDTO commentDTO = createCommentDTO();
        Post post = createPost();

        // Act
        Mockito.when(postsService.getPostById(1)).thenReturn(post);
        Mockito.when(principal.getName()).thenReturn("test@test.com");
        Mockito.when(usersService.getByEmail("test@test.com")).thenReturn(userInfo);

        Comment comment = commentsService.create(commentDTO,principal);
        // Assert
        Mockito.verify(repository, Mockito.times(1)).save(comment);
    }

    @Test
    public void updateShould_CallRepository_WhenCommentExists() {
        // Arrange
        Comment comment = createComment();
        CommentDTO commentDTO = createCommentDTO();

        // Act
        Mockito.when(repository.getCommentById(anyInt())).thenReturn(comment);
        Mockito.when(repository.existsById(anyInt())).thenReturn(true);
        commentsService.update(comment.getId(), commentDTO);

        // Assert
        Mockito.verify(repository, Mockito.times(1)).save(comment);
    }

    @Test
    public void updateShould_ThrowExceptionWhen_CommentDoNotExist() {
        // Arrange
        CommentDTO comment = createCommentDTO();


        // Act
        Mockito.when(repository.existsById(anyInt())).thenReturn(false);

        // Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> commentsService.update(anyInt(),comment));
    }

    @Test
    public void deleteShould_CallRepository() {
        // Arrange
        Comment comment = createComment();
        Post post = createPost();

        // Act
        Mockito.when(repository.existsById(anyInt())).thenReturn(true);
        Mockito.when(postsService.getPostById(anyInt())).thenReturn(post);
        commentsService.delete(comment);

        // Assert
        Mockito.verify(repository, Mockito.times(1)).delete(comment);
    }

    @Test
    public void deleteShould_ThrowExceptionWhen_CommentDoNotExist() {
        // Arrange
        Comment comment = createComment();

        // Act
        Mockito.when(repository.existsById(anyInt())).thenReturn(false);

        // Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> commentsService.delete(comment));
    }

    @Test
    public void getCommentByIdShould_ReturnComment_WhenCommentExists() {
        // Arrange
        Comment expected = createComment();

        // Act
        Mockito.when(repository.existsById(anyInt())).thenReturn(true);
        Mockito.when(repository.getCommentById(1))
                .thenReturn(expected);
        Comment returned = commentsService.getCommentById(1);

        // Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void getCommentByIdShould_ThrowException_WhenCommentDoesNotExist() {
        // Arrange
        Mockito.when(repository.existsById(anyInt()))
                .thenReturn(false);

        // Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> commentsService.getCommentById(anyInt()));
    }

    @Test
    public void getAllCommentsShould_CallRepository() {
        // Arrange
        Comment expected = createComment();
        Mockito.when(repository.findAll())
                .thenReturn(Collections.singletonList(expected));

        // Act
        List<Comment> returnedComments = commentsService.getAllComments();
        // Assert
        Assert.assertSame(1, returnedComments.size());
    }

    @Test
    public void getCommentsForPostShould_CallRepository() {
        // Arrange
        Comment comment = createComment();
        Post post = createPost();
        Mockito.when(repository.getAllByPostId(post.getId())).
                thenReturn(Collections.singletonList(comment));

        // Act
        List<Comment> returnedComments = commentsService.getCommentsForPost(post.getId());

        // Assert

        Assert.assertSame(1, returnedComments.size());
    }

    @Test
    public  void editCommentShould_CallRepository(){
        //Arrange
        CommentEditDTO model = createEditCommentModel();
        Comment comment = createComment();

        //Act
        Mockito.when(repository.getCommentById(anyInt())).thenReturn(comment);
        commentsService.editComment(model);
        //Assert
        Mockito.verify(repository, Mockito.times(1)).save(comment);
    }
}
