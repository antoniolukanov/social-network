package com.team12.socialnetwork.services;

import com.team12.socialnetwork.entities.Friend;
import com.team12.socialnetwork.entities.Request;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.exceptions.DuplicateEntityException;
import com.team12.socialnetwork.repositories.base.RequestsRepository;
import com.team12.socialnetwork.services.base.FriendsService;
import com.team12.socialnetwork.services.base.UsersService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.Principal;

import static com.team12.socialnetwork.Factory.*;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class RequestsServiceImplTests {
    @Mock
    private RequestsRepository requestsRepository;

    @Mock
    private FriendsService friendsService;

    @Mock
    private Principal principal;

    @Mock
    private UsersService usersService;

    @InjectMocks
    private RequestsServiceImpl mockService;

    @Test
    public void getBySenderIdAndReceiverIdShould_ReturnRequest_WhenRequestExists() {
        // Arrange
        Request expected = createRequest();
        int senderId = expected.getSender().getId();
        int receiverId = expected.getReceiver().getId();

        Mockito.when(requestsRepository.getBySenderIdAndReceiverId(senderId, receiverId))
                .thenReturn(expected);

        // Act
        Request returned = mockService.getBySenderIdAndReceiverId(senderId, receiverId);

        // Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void getAllBySenderIdShould_CallRepository() {
        //Arrange
        mockService.getAllBySenderId(1);

        //Act, Assert
        Mockito.verify(requestsRepository, times(1))
                .getAllBySenderId(1);
    }

    @Test
    public void getAllByReceiverIdShould_CallRepository() {
        //Arrange
        mockService.getAllByReceiverId(1);

        //Act, Assert
        Mockito.verify(requestsRepository, times(1))
                .getAllByReceiverId(1);
    }

    @Test
    public void sendFriendRequestShould_CreateNewRequest() {
        //Arrange
        UserInfo sender = createUser();
        UserInfo receiver = createUserTwo();

        //Act
        Mockito.when(usersService.getByEmail(principal.getName())).thenReturn(sender);
        Mockito.when(usersService.getUserById(receiver.getId())).thenReturn(receiver);
        Mockito.when(friendsService.getBySenderIdAndReceiverId(sender.getId(),receiver.getId())).thenReturn(null);

        mockService.sendFriendRequest(principal, receiver.getId());

        //Assert
        Mockito.verify(requestsRepository, Mockito.times(1)).save(Mockito.any());
    }

    @Test
    public void sendFriendRequestShould_ThrowException_WhenRequestExists() {
        //Arrange
        UserInfo sender = createUser();
        UserInfo receiver = createUserTwo();
        Request request = createRequest();

        //Act
        Mockito.when(usersService.getByEmail(principal.getName())).thenReturn(sender);
        Mockito.when(usersService.getUserById(receiver.getId())).thenReturn(receiver);
        Mockito.when(requestsRepository.getBySenderIdAndReceiverId(sender.getId(), receiver.getId())).thenReturn(request);

        //Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.sendFriendRequest(principal, receiver.getId()));
    }

    @Test
    public void sendFriendRequestShould_ThrowException_WhenFriendExists() {
        //Arrange
        UserInfo sender = createUser();
        UserInfo receiver = createUserTwo();
        Friend friend = createFriend();

        //Act
        Mockito.when(usersService.getByEmail(principal.getName())).thenReturn(sender);
        Mockito.when(usersService.getUserById(receiver.getId())).thenReturn(receiver);
        Mockito.when(requestsRepository.getBySenderIdAndReceiverId(sender.getId(), receiver.getId())).thenReturn(null);
        Mockito.when(friendsService.getBySenderIdAndReceiverId(sender.getId(), receiver.getId())).thenReturn(friend);


        //Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.sendFriendRequest(principal, receiver.getId()));
    }

    @Test
    public void acceptFriendRequestShould_CreateNewFriend() {
        //Arrange
        UserInfo sender = createUser();
        UserInfo receiver = createUserTwo();

        //Act
        Mockito.when(usersService.getByEmail(principal.getName())).thenReturn(sender);
        Mockito.when(usersService.getUserById(receiver.getId())).thenReturn(receiver);

        mockService.acceptFriendRequest(principal, receiver.getId());

        //Assert
        Mockito.verify(friendsService, Mockito.times(2)).createFriendship(Mockito.any());
    }

    @Test
    public void acceptFriendRequestShould_ThrowException_WhenRequestDoNotExists() {
        //Arrange
        UserInfo sender = createUser();
        UserInfo receiver = createUserTwo();
        Friend friend = createFriend();

        //Act
        Mockito.when(usersService.getByEmail(principal.getName())).thenReturn(receiver);
        Mockito.when(usersService.getUserById(sender.getId())).thenReturn(sender);
        Mockito.when(friendsService.getBySenderIdAndReceiverId(sender.getId(), receiver.getId())).thenReturn(friend);

        //Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.acceptFriendRequest(principal, sender.getId()));
    }

    @Test
    public void declineFriendRequestShould_DeleteRequest() {
        //Arrange
        UserInfo sender = createUser();
        UserInfo receiver = createUserTwo();
        Request request = createRequest();

        //Act
        Mockito.when(usersService.getByEmail(principal.getName())).thenReturn(receiver);
        Mockito.when(usersService.getUserById(sender.getId())).thenReturn(sender);
        Mockito.when(requestsRepository.getBySenderIdAndReceiverId(sender.getId(), receiver.getId())).thenReturn(request);

        mockService.declineFriendRequest(principal, sender.getId());

        //Assert
        Mockito.verify(requestsRepository, Mockito.times(1)).delete(Mockito.any());
    }
}
