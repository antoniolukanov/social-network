package com.team12.socialnetwork.services;

import com.team12.socialnetwork.entities.DTOmodels.LikePostDTO;
import com.team12.socialnetwork.entities.LikePost;
import com.team12.socialnetwork.entities.Post;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.exceptions.DuplicateEntityException;
import com.team12.socialnetwork.repositories.base.LikePostRepository;
import com.team12.socialnetwork.repositories.base.PostsRepository;
import com.team12.socialnetwork.services.base.PostsService;
import com.team12.socialnetwork.services.base.UsersService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.security.Principal;

import static com.team12.socialnetwork.Factory.*;

@RunWith(MockitoJUnitRunner.class)
public class LikePostServiceImplTests {

    @Mock
    private LikePostRepository repository;

    @Mock
    private Principal principal;

    @Mock
    private UsersService usersService;

    @Mock
    private PostsRepository postsRepository;

    @Mock
    private PostsService postsService;

    @InjectMocks
    private LikePostServiceImpl mockService;

    @Test
    public void createShould_CallRepository() {
        // Arrange
        LikePost expected = createLikePost();

        // Act
        mockService.createLikePost(expected);

        // Assert
        Mockito.verify(repository, Mockito.times(1)).save(expected);
    }

    @Test
    public void deleteShould_CallRepository() {
        // Arrange
        LikePost expected = createLikePost();

        // Act
        mockService.deleteLikePost(expected);

        // Assert
        Mockito.verify(repository, Mockito.times(1)).delete(expected);
    }

    @Test
    public void likePostShould_IncreaseLikeCount() {
        // Arrange
        Post post = createPost();
        UserInfo user = createUser();
        LikePostDTO likePostDTO = createLikePostDTO();

        // Act
        Mockito.when(postsService.getPostById(likePostDTO.getPostId())).thenReturn(post);
        Mockito.when(usersService.getByEmail(principal.getName())).thenReturn(user);
        mockService.likePost(likePostDTO,principal);

        // Assert
        Assert.assertEquals(1,post.getTotalLikes());
    }

    @Test
    public void existsByUserAndPost_ThrowException_WhenLikePostAlreadyExists(){
        // Arrange
        Post post = createPost();
        UserInfo user = createUser();
        LikePost likePost = createLikePost();
        LikePostDTO likePostDTO = createLikePostDTO();

        // Act
        Mockito.when(postsService.getPostById(likePostDTO.getPostId())).thenReturn(post);
        Mockito.when(usersService.getByEmail(principal.getName())).thenReturn(user);
        Mockito.when(repository.existsByUserAndPost(user,post)).thenReturn(true);

        // Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () ->mockService.likePost(likePostDTO,principal));
    }

    @Test
    public void dislikePostShould_DecreaseLikeCount(){
        // Arrange
        Post post = createPost();
        UserInfo user = createUser();
        LikePostDTO likePostDTO = createLikePostDTO();
        LikePost likePost = createLikePost();

        // Act
        Mockito.when(postsService.getPostById(likePostDTO.getPostId())).thenReturn(post);
        Mockito.when(usersService.getByEmail(principal.getName())).thenReturn(user);
        Mockito.when(repository.getByUserAndPost(user,post)).thenReturn(likePost);
        mockService.dislikePost(likePostDTO,principal);

        // Assert
        Assert.assertEquals(-1,post.getTotalLikes());
    }
}
