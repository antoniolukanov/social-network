package com.team12.socialnetwork.services;

import com.team12.socialnetwork.entities.DTOmodels.AdditionalInfoDTO;
import com.team12.socialnetwork.entities.DTOmodels.PasswordChangeDTO;
import com.team12.socialnetwork.entities.DTOmodels.RegistrationDTO;
import com.team12.socialnetwork.entities.Login;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.exceptions.DuplicateEntityException;
import com.team12.socialnetwork.mapper.AdditionalInfoMapper;
import com.team12.socialnetwork.mapper.UserMapper;
import com.team12.socialnetwork.repositories.base.UsersRepository;
import com.team12.socialnetwork.services.base.LoginService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.EntityNotFoundException;
import java.util.Collections;
import java.util.List;

import static com.team12.socialnetwork.Factory.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class UsersServiceImplTests {
    @Mock
    private UsersRepository repository;

    @Mock
    private UserMapper userMapper;

    @Mock
    private LoginService loginService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Mock
    private AdditionalInfoMapper additionalInfoMapper;

    @InjectMocks
    private UsersServiceImpl mockService;

    @Test
    public void createShould_CallRepository_WhenUserNotExists() {
        // Arrange
        UserInfo expected = createUser();
        RegistrationDTO dto = createRegModel();
        Login login = createLogin();

        // Act
        Mockito.when(loginService.existsByUsername(anyString())).thenReturn(false);
        Mockito.when(loginService.findByUsername(anyString())).thenReturn(login);
        Mockito.when(userMapper.registrationModelToUserInfo(dto)).thenReturn(expected);
        mockService.create(dto);

        // Assert
        Mockito.verify(repository,
                times(1)).save(expected);
    }

    @Test
    public void createShould_ThrowException_WhenUserAlreadyExists() {
        // Arrange
        RegistrationDTO dto = createRegModel();

        // Act
        Mockito.when(loginService.existsByUsername(anyString())).thenReturn(true);

        //Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(dto));
    }

    @Test
    public void updateShould_CallRepository_WhenUserExists() {
        // Arrange
        UserInfo expected = createUser();
        AdditionalInfoDTO dto = createAddInfoDTO();
        Login login = createLogin();

        // Act
        Mockito.when(repository.getByLoginUsername(anyString())).thenReturn(expected);
        mockService.update(dto, anyString());

        // Assert
        Mockito.verify(repository,
                times(1)).save(expected);
    }

    @Test
    public void deleteShould_CallRepository_WhenUserExists() {
        // Arrange
        UserInfo expected = createUser();

        // Act
        mockService.delete(expected);

        // Assert
        Mockito.verify(repository,
                times(1)).delete(expected);
    }

    @Test
    public void getUserByIdShould_ReturnUser_WhenUserExists() {
        // Arrange
        UserInfo expected = createUser();


        // Act
        Mockito.when(repository.getById(1))
                .thenReturn(expected);
        UserInfo returned = mockService.getUserById(1);

        // Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void getByIdShould_ThrowException_WhenUserDoNotExists() {
        // Arrange, Act
        Mockito.when(repository.getById(anyInt()))
                .thenReturn(null);

        // Assert
        Assert.assertNull(repository.getById(anyInt()));
    }

    @Test
    public void getUserByEmailShould_ReturnUser_WhenUserExists() {
        // Arrange
        UserInfo expected = createUser();

        // Act
        Mockito.when(repository.getByLoginUsername("test@test.com"))
                .thenReturn(expected);
        UserInfo returned = mockService.getByEmail("test@test.com");

        // Assert
        Assert.assertSame(expected, returned);
    }

    @Test
    public void getByEmailShould_ThrowException_WhenUserDoNotExists() {
        // Arrange, Act
        Mockito.when(repository.getByLoginUsername(anyString()))
                .thenReturn(null);

        // Assert
        Assert.assertNull(repository.getByLoginUsername(anyString()));
    }

    @Test
    public void getAllShould_CallRepository() {
        // Arrange
        UserInfo expected = createUser();
        Mockito.when(repository.findAll())
                .thenReturn(Collections.singletonList(expected));

        // Act
        List<UserInfo> returnedUserInfos = mockService.getAll();
        // Assert
        Assert.assertSame(1, returnedUserInfos.size());
    }

    @Test
    public void checkIfUserExistsShould_returnTrueWhen_UserExists() {
        //Arrange
        //Act
        Mockito.when(repository.existsById(anyInt())).thenReturn(true);

        //Assert
        Assert.assertTrue(mockService.checkIfUserExists(anyInt()));
    }

    @Test
    public void adminUpdateShould_CallRepository_WhenUserExists() {
        // Arrange
        UserInfo expected = createUser();
        RegistrationDTO registrationDTO = createRegModel();

        // Act
        Mockito.when(repository.getByLoginUsername(anyString())).thenReturn(expected);
        mockService.adminUpdate(registrationDTO, anyString());

        // Assert
        Mockito.verify(repository,
                times(1)).save(expected);
    }

    @Test
    public void changePasswordShould_changePWWhen_UserExists(){
        // Arrange
        UserInfo expected = createUser();
        Login login = expected.getLogin();
        PasswordChangeDTO passwordChangeDTO = createPWChangeModel();

        // Act
        Mockito.when(repository.getById(anyInt())).thenReturn(expected);
        mockService.changePassword(passwordChangeDTO);

        Mockito.verify(loginService,
                times(1)).updatePassword(login);
    }

    @Test
    public void getByFirstOrLastName_ShouldCallRepository(){
        // Arrange
        UserInfo expected = createUser();
        Mockito.when(repository.findByFirstNameStartingWithOrLastNameStartingWith(anyString(),anyString()))
                .thenReturn(Collections.singletonList(expected));

        // Act
        List<UserInfo> returnedUserInfos = mockService.getAllUsersByFirstNameOrLastName(anyString(),anyString());
        // Assert
        Assert.assertSame(1, returnedUserInfos.size());
    }
}
