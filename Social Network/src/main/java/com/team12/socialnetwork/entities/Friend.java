package com.team12.socialnetwork.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "friends")
public class Friend {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int friendId;

    @ManyToOne
    @JoinColumn(name = "sender_id")
    private UserInfo sender;

    @ManyToOne
    @JoinColumn(name = "receiver_id")
    private UserInfo receiver;

    public Friend(UserInfo sender, UserInfo receiver) {
        this.sender = sender;
        this.receiver = receiver;
    }
}
