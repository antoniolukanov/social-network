package com.team12.socialnetwork.entities.DTOmodels;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.PositiveOrZero;

@Getter
@Setter
@NoArgsConstructor
public class RelationshipDTO {

    @PositiveOrZero(message = "senderId should be positive")
    private int senderId;

    @PositiveOrZero(message = "receiverId should be positive")
    private int receiverId;

    @PositiveOrZero(message = "statusId should be positive or zero")
    private int statusId;
}
