package com.team12.socialnetwork.entities.DTOmodels;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class PasswordChangeDTO {

    @PositiveOrZero
    private int userId;

    String currentPassword;

    @Size(min = 8, max = 40, message = "Password  should be between 8 and 40 symbols.")
    String newPassword;

    @Size(min = 8, max = 40, message = "Password  should be between 8 and 40 symbols.")
    String confirmPassword;
}
