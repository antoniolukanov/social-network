package com.team12.socialnetwork.entities.enums;

public enum PrivacyType {
    PUBLIC,
    PRIVATE
}
