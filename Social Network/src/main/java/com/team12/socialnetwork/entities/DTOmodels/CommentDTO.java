package com.team12.socialnetwork.entities.DTOmodels;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor
public class CommentDTO {

    @NotBlank
    private String content;

    private int postId;
}
