package com.team12.socialnetwork.entities.DTOmodels;

import com.team12.socialnetwork.entities.enums.PrivacyType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class UserDTO {

    @NotBlank
    @Size(min = 1, max = 30, message = "First name should be between 1 and 30 symbols.")
    private String firstName;

    @NotBlank
    @Size(min = 1, max = 30, message = "Last name should be between 1 and 30 symbols.")
    private String lastName;

    @NotBlank
    @Size(min = 5, max = 60, message = "Email name should be between 5 and 60 symbols.")
    private String email;

    @NotBlank
    @Size(min = 8, max = 40, message = "Password  should be between 8 and 40 symbols.")
    private String password;

    private PrivacyType privacy;
}
