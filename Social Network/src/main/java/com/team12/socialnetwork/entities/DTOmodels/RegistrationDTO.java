package com.team12.socialnetwork.entities.DTOmodels;

import com.team12.socialnetwork.entities.enums.PrivacyType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RegistrationDTO {

    @Size(min = 2, message = "First name is required")
    private String firstName;

    @Size(min = 2, message = "Last name is required")
    private String lastName;

    @Size(min = 8, message = "Password is required")
    private String password;

    @Size(min = 8, message = "Password is required")
    private String confirmPassword;

    private String email;

    private PrivacyType privacy;

    private int age;

    private String address;

    private String education;
}
