package com.team12.socialnetwork.entities.DTOmodels;

import com.team12.socialnetwork.entities.enums.PrivacyType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
public class PostDTO {

    @NotBlank
    @Size(min = 1, max = 255, message = "Comment should be between 2 and 255 symbols")
    private String content;

    private String pictureValue;

    private String videoPost;

    private String locationPost;

    private PrivacyType privacy;

    private int creatorId;
}
