package com.team12.socialnetwork.entities.DTOmodels;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.PositiveOrZero;

@Getter
@Setter
@NoArgsConstructor
public class LikePostDTO {

    @PositiveOrZero
    private int postId;
}
