package com.team12.socialnetwork.controllers.rest;

import com.team12.socialnetwork.entities.DTOmodels.AdditionalInfoDTO;
import com.team12.socialnetwork.entities.DTOmodels.PasswordChangeDTO;
import com.team12.socialnetwork.entities.DTOmodels.RegistrationDTO;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.exceptions.DuplicateEntityException;
import com.team12.socialnetwork.services.base.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
public class UsersRestController {
    private final UsersService usersService;

    @GetMapping
    public List<UserInfo> getAllUsers() {
        return usersService.getAll();
    }

    @GetMapping("/{id}")
    public UserInfo getUserById(@PathVariable("id") int id) {
        try {
            return usersService.getUserById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public UserInfo create(@RequestBody @Valid RegistrationDTO RegistrationDTO) {
        try {
            return usersService.create(RegistrationDTO);
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping
    public UserInfo updateUserInfo(Principal principal,
                                   @RequestBody @Valid AdditionalInfoDTO dto) {
        try {
            return usersService.update(dto, principal.getName());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/edit-pass")
    public void changeUserPassword(@RequestBody @Valid
                                           PasswordChangeDTO model) {
        try {
            usersService.changePassword(model);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            usersService.delete(usersService.getUserById(id));
        } catch (EntityNotFoundException nfe) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, nfe.getMessage());
        }
    }

    @GetMapping("/filter")
    public List<UserInfo> getAllUsersByFirstNameOrLastName(@RequestParam(required = false) String firstName,
                                                           @RequestParam(required = false) String lastName) {
        return usersService.getAllUsersByFirstNameOrLastName(firstName, lastName);
    }
}
