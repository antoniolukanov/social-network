package com.team12.socialnetwork.controllers.rest;

import com.team12.socialnetwork.entities.DTOmodels.LikePostDTO;
import com.team12.socialnetwork.exceptions.DuplicateEntityException;
import com.team12.socialnetwork.services.base.LikePostService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/postlikes")
public class LikePostRestController {

    private final LikePostService likePostService;

    @PostMapping("/like")
    public int likePost(@RequestBody @Valid LikePostDTO likePostDTO, Principal principal) {
        try {
            return likePostService.likePost(likePostDTO, principal);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PostMapping("/dislike")
    public int unlikePost(@RequestBody @Valid LikePostDTO likePostDTO, Principal principal) {
        try {
            return likePostService.dislikePost(likePostDTO, principal);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
