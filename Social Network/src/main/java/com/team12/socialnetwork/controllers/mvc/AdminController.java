package com.team12.socialnetwork.controllers.mvc;

import com.team12.socialnetwork.entities.DTOmodels.CommentEditDTO;
import com.team12.socialnetwork.entities.DTOmodels.PostEditDTO;
import com.team12.socialnetwork.entities.DTOmodels.RegistrationDTO;
import com.team12.socialnetwork.entities.Comment;
import com.team12.socialnetwork.entities.Post;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.services.base.CommentsService;
import com.team12.socialnetwork.services.base.PostsService;
import com.team12.socialnetwork.services.base.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
@RequiredArgsConstructor
public class AdminController {

    private final PostsService postsService;
    private final UsersService usersService;
    private final CommentsService commentsService;

    @GetMapping("/admin")
    public String showAdminPage(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserInfo userInfo = usersService.getByEmail(authentication.getName());
        model.addAttribute("usersAdmin", usersService.getAll());
        model.addAttribute("postsAdmin", postsService.getAll());
        model.addAttribute("commentsAdmin", commentsService.getAllComments());
        model.addAttribute("userInfo", userInfo);

        return "admin";
    }

    @PostMapping("/admin")
    public String editUser(@Valid @ModelAttribute("regUser") RegistrationDTO registrationDTO,
                           BindingResult errors) {
        if (errors.hasErrors()) {
            return "admin";
        }
        usersService.adminUpdate(registrationDTO, registrationDTO.getEmail());

        return "redirect:/admin";
    }


    @PostMapping("/admin/delete-user")
    public String deleteUser(@Valid @ModelAttribute("regUser") RegistrationDTO registrationDTO,
                             BindingResult errors) {
        if (errors.hasErrors()) {
            return "admin";
        }

        UserInfo user = usersService.getByEmail(registrationDTO.getEmail());
        usersService.delete(user);

        return "redirect:/admin";
    }

    @PostMapping("/admin/posts")
    public String editPost(@Valid @ModelAttribute("postModel") PostEditDTO model,
                           BindingResult errors) {
        if (errors.hasErrors()) {
            return "admin";
        }

        postsService.editPost(model);

        return "redirect:/admin";
    }

    @PostMapping("/admin/delete-post")
    public String deletePost(@Valid @ModelAttribute("postModel") PostEditDTO postEditDTO,
                             BindingResult errors) {
        if (errors.hasErrors()) {
            return "admin";
        }

        Post post = postsService.getPostById(postEditDTO.getId());
        postsService.delete(post);

        return "redirect:/admin";
    }

    @PostMapping("/admin/comments")
    public String editComment(@Valid @ModelAttribute("commentModel") CommentEditDTO commentEditDTO,
                              BindingResult errors) {
        if (errors.hasErrors()) {
            return "admin";
        }

        commentsService.editComment(commentEditDTO);

        return "redirect:/admin";
    }

    @PostMapping("/admin/delete-comment")
    public String deleteComment(@Valid @ModelAttribute("commentModel") CommentEditDTO commentEditDTO,
                                BindingResult errors) {
        if (errors.hasErrors()) {
            return "admin";
        }

        Comment comment = commentsService.getCommentById(commentEditDTO.getId());
        commentsService.delete(comment);

        return "redirect:/admin";

    }
}
