package com.team12.socialnetwork.repositories.base;

import com.team12.socialnetwork.entities.Comment;
import com.team12.socialnetwork.entities.LikeComment;
import com.team12.socialnetwork.entities.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LikeCommentRepository extends JpaRepository<LikeComment, Integer> {

    LikeComment getByUserAndComment(UserInfo user, Comment comment);

    boolean existsByUserAndComment(UserInfo user, Comment comment);
}
