package com.team12.socialnetwork.repositories.base;

import com.team12.socialnetwork.entities.Login;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoginRepository extends JpaRepository<Login, String> {

    boolean existsByUsername(String email);
}
