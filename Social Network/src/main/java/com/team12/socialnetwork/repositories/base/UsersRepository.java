package com.team12.socialnetwork.repositories.base;

import com.team12.socialnetwork.entities.UserInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends JpaRepository<UserInfo, Integer> {

    UserInfo getById(int id);

    UserInfo getByLoginUsername(String email);

    List<UserInfo> findByFirstNameStartingWithOrLastNameStartingWith(String firstName, String lastName);
}
