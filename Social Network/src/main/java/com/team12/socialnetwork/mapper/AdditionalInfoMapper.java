package com.team12.socialnetwork.mapper;

import com.team12.socialnetwork.entities.DTOmodels.AdditionalInfoDTO;
import com.team12.socialnetwork.entities.UserInfo;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;

@Mapper
public interface AdditionalInfoMapper {

    void AdditionalInfoDTOToUserInfo(AdditionalInfoDTO additionalInfoDTO, @MappingTarget UserInfo userInfo);
}
