package com.team12.socialnetwork.services.base;

import com.team12.socialnetwork.entities.DTOmodels.PostEditDTO;
import com.team12.socialnetwork.entities.DTOmodels.PostDTO;
import com.team12.socialnetwork.entities.Post;
import com.team12.socialnetwork.entities.enums.PrivacyType;

import java.security.Principal;
import java.util.List;

public interface PostsService {

    Post create(PostDTO postDTO, Principal principal);

    Post update(int id, PostDTO postDTO);

    void delete(Post post);

    List<Post> getAllByCreatorSorted(int id);

    List<Post> getAllByPrivacyOrderByTotalInteractionsDesc(PrivacyType privacyType);

    List<Post> getPublicPosts();

    Post getPostById(int postId);

    List<Post> getCreatorPosts(int id, Principal principal);

    Post editPost(PostEditDTO postEditDTO);

    List<Post> getAll();

    List<Post> getAllFriendsPosts(Principal principal);
}
