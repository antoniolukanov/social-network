package com.team12.socialnetwork.services;

import com.team12.socialnetwork.entities.DTOmodels.RegistrationDTO;
import com.team12.socialnetwork.entities.Login;
import com.team12.socialnetwork.repositories.base.LoginRepository;
import com.team12.socialnetwork.services.base.LoginService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LoginServiceImpl implements LoginService {

    private final LoginRepository loginRepository;
    private final UserDetailsManager userDetailsManager;
    private final PasswordEncoder passwordEncoder;

    @Override
    public Login findByUsername(String username) {
        return loginRepository.findById(username).orElse(null);
    }

    @Override
    public void createLogin(RegistrationDTO registrationDTO) {
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        registrationDTO.getEmail(), passwordEncoder.encode(registrationDTO.getPassword()), authorities);

        userDetailsManager.createUser(newUser);
    }

    @Override
    public boolean existsByUsername(String email) {
        return loginRepository.existsByUsername(email);
    }

    @Override
    public void updatePassword(Login login) {
        loginRepository.save(login);
    }
}