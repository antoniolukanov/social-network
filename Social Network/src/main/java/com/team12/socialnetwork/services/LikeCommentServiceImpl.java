package com.team12.socialnetwork.services;

import com.team12.socialnetwork.entities.DTOmodels.LikeCommentDTO;
import com.team12.socialnetwork.entities.Comment;
import com.team12.socialnetwork.entities.LikeComment;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.exceptions.DuplicateEntityException;
import com.team12.socialnetwork.repositories.base.CommentsRepository;
import com.team12.socialnetwork.repositories.base.LikeCommentRepository;
import com.team12.socialnetwork.services.base.CommentsService;
import com.team12.socialnetwork.services.base.LikeCommentService;
import com.team12.socialnetwork.services.base.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.security.Principal;

@Service
@RequiredArgsConstructor
public class LikeCommentServiceImpl implements LikeCommentService {

    private final LikeCommentRepository likeCommentRepository;
    private final CommentsRepository commentsRepository;
    private final CommentsService commentsService;
    private final UsersService usersService;

    private static final String COMMENT_ALREADY_LIKED = "You have already liked this comment!";

    @Override
    public void createLikeComment(LikeComment likeComment) {
        likeCommentRepository.save(likeComment);
    }

    @Override
    public void deleteLikeComment(LikeComment likeComment) {
        likeCommentRepository.delete(likeComment);
    }

    @Override
    public int likeComment(LikeCommentDTO likeCommentDTO, Principal principal) {
        Comment comment = commentsService.getCommentById(likeCommentDTO.getCommentId());
        UserInfo user = usersService.getByEmail(principal.getName());

        if (likeCommentRepository.existsByUserAndComment(user, comment)) {
            throw new DuplicateEntityException(COMMENT_ALREADY_LIKED);
        }

        LikeComment likeComment = new LikeComment(user, comment);
        createLikeComment(likeComment);
        comment.setTotalLikes(comment.getTotalLikes() + 1);
        commentsRepository.save(comment);

        return comment.getTotalLikes();
    }


    @Override
    public int dislikeComment(LikeCommentDTO likeCommentDTO, Principal principal) {
        Comment comment = commentsService.getCommentById(likeCommentDTO.getCommentId());
        UserInfo user = usersService.getByEmail(principal.getName());

        if (!likeCommentRepository.existsByUserAndComment(user, comment)) {
            throw new EntityNotFoundException("This post has not been liked yet!");
        }

        LikeComment unlike = likeCommentRepository.getByUserAndComment(user, comment);
        deleteLikeComment(unlike);
        comment.setTotalLikes(comment.getTotalLikes() - 1);
        commentsRepository.save(comment);

        return comment.getTotalLikes();
    }
}
