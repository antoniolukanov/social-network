package com.team12.socialnetwork.services.base;

import com.team12.socialnetwork.entities.Friend;

import java.security.Principal;
import java.util.List;

public interface FriendsService {

    void createFriendship(Friend friend);

    void deleteFriendship(Friend friend);

    Friend getBySenderIdAndReceiverId(int userId, int friendId);

    List<Friend> getAllBySenderId(int senderId);

    List<Friend> getAllByReceiverId(int receiverId);

    void deleteFriend(Principal principal, int friendId);
}
