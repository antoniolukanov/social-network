package com.team12.socialnetwork.services.base;

import com.team12.socialnetwork.entities.DTOmodels.CommentDTO;
import com.team12.socialnetwork.entities.DTOmodels.CommentEditDTO;
import com.team12.socialnetwork.entities.Comment;

import java.security.Principal;
import java.util.List;

public interface CommentsService {

    Comment create(CommentDTO commentDTO, Principal principal);

    Comment update(int id, CommentDTO commentDTO);

    void editComment(CommentEditDTO editModel);

    void delete(Comment comment);

    Comment getCommentById(int commentId);

    List<Comment> getAllComments();

    List<Comment> getCommentsForPost(int postId);
}
