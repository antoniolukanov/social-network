package com.team12.socialnetwork.services;

import com.team12.socialnetwork.entities.DTOmodels.CommentEditDTO;
import com.team12.socialnetwork.entities.DTOmodels.CommentDTO;
import com.team12.socialnetwork.entities.Comment;
import com.team12.socialnetwork.entities.Post;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.repositories.base.CommentsRepository;
import com.team12.socialnetwork.repositories.base.PostsRepository;
import com.team12.socialnetwork.services.base.CommentsService;
import com.team12.socialnetwork.services.base.PostsService;
import com.team12.socialnetwork.services.base.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.persistence.PrePersist;
import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class CommentsServiceImpl implements CommentsService {

    private final CommentsRepository commentRepository;
    private final UsersService usersService;
    private final PostsService postsService;
    private final PostsRepository postsRepository;

    private static final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private final static String COMMENT_NOT_FOUND = "Comment with id %d does not exist!";

    @Override
    public Comment create(CommentDTO commentDTO, Principal principal) {
        Comment comment = new Comment();
        Post post = postsService.getPostById(commentDTO.getPostId());
        UserInfo creator = usersService.getByEmail(principal.getName());
        comment.setTimeCreated(onCreate());
        comment.setContent(commentDTO.getContent());
        comment.setPost(post);
        comment.setCreator(creator);
        post.setTotalInteractions(post.getTotalInteractions() + 1);
        commentRepository.save(comment);
        return comment;
    }

    @Override
    public Comment update(int id, CommentDTO commentDTO) {
        if (!commentRepository.existsById(id)) {
            throw new EntityNotFoundException(format(COMMENT_NOT_FOUND, id));
        }

        Comment toUpdate = getCommentById(id);
        toUpdate.setContent(commentDTO.getContent());
        return commentRepository.save(toUpdate);
    }

    @Override
    public void editComment(CommentEditDTO editModel) {
        Comment comment = commentRepository.getCommentById(editModel.getId());
        comment.setContent(editModel.getContent());
        comment.setTotalLikes(editModel.getTotalLikes());
        commentRepository.save(comment);
    }

    @Override
    public void delete(Comment comment) {
        if (!commentRepository.existsById(comment.getId())) {
            throw new EntityNotFoundException(format(COMMENT_NOT_FOUND, comment.getId()));
        }
        Post post = postsService.getPostById(comment.getPost().getId());
        post.setTotalInteractions(post.getTotalInteractions() - 1);
        postsRepository.save(post);
        commentRepository.delete(comment);
    }

    @Override
    public Comment getCommentById(int commentId) {
        if (!commentRepository.existsById(commentId)) {
            throw new EntityNotFoundException(format(COMMENT_NOT_FOUND, commentId));
        }

        return commentRepository.getCommentById(commentId);
    }

    @Override
    public List<Comment> getAllComments() {
        return commentRepository.findAll();
    }

    @Override
    public List<Comment> getCommentsForPost(int postId) {
        return commentRepository.getAllByPostId(postId);
    }

    @PrePersist
    protected String onCreate() {
        LocalDateTime localDate = LocalDateTime.now();
        return DateTimeFormatter.ofPattern(TIME_FORMAT).format(localDate);
    }
}
