package com.team12.socialnetwork.services;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import com.team12.socialnetwork.entities.DTOmodels.PostDTO;
import com.team12.socialnetwork.entities.DTOmodels.PostEditDTO;
import com.team12.socialnetwork.entities.Friend;
import com.team12.socialnetwork.entities.Post;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.entities.enums.PrivacyType;
import com.team12.socialnetwork.mapper.PostMapper;
import com.team12.socialnetwork.repositories.base.PostsRepository;
import com.team12.socialnetwork.services.base.FriendsService;
import com.team12.socialnetwork.services.base.PostsService;
import com.team12.socialnetwork.services.base.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.persistence.PrePersist;
import java.security.Principal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
public class PostsServiceImpl implements PostsService {

    private final PostsRepository postRepository;
    private final UsersService usersService;
    private final PostMapper postMapper;
    private final FriendsService friendsService;

    private static final String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    private static final String USER_NOT_EXIST = "User with id %d do not exist!";
    private static final String POST_NOT_EXIST = "Post with id %d do not exist!";

    @Override
    public Post create(PostDTO postDTO, Principal principal) {
        String result = "";
        if (postDTO.getPictureValue() != null) {
            String imageData = postDTO.getPictureValue();
            result = imageData.replaceFirst("^data:image/[^;]*;base64,?", "");
        }
        byte[] decodedByte = Base64.decode(result);
        Post newPost = postMapper.PostDTOToPost(postDTO);
        UserInfo userInfo = usersService.getByEmail(principal.getName());
        newPost.setCreatedBy(userInfo);
        newPost.setTimeCreated(onCreate());
        newPost.setPicturePost(decodedByte);

        return postRepository.save(newPost);
    }

    @Override
    public Post update(int id, PostDTO postDTO) {
        Post toUpdate = postRepository.getPostById(id);
        postMapper.PostDTOToPostUpdate(postDTO, toUpdate);

        if (!usersService.checkIfUserExists(toUpdate.getCreatedBy().getId())) {
            throw new EntityNotFoundException(format(USER_NOT_EXIST, toUpdate.getCreatedBy().getId()));
        }
        return postRepository.save(toUpdate);
    }

    @Override
    public void delete(Post post) {
        if (!postRepository.existsById(post.getId())) {
            throw new EntityNotFoundException(format(POST_NOT_EXIST, post.getId()));
        }
        postRepository.delete(post);
    }


    @Override
    public List<Post> getAllByCreatorSorted(int id) {
        if (!usersService.checkIfUserExists(id)) {
            throw new EntityNotFoundException(String.format(USER_NOT_EXIST, id));
        }

        UserInfo creator = usersService.getUserById(id);
        return postRepository.getAllByCreatedByOrderByTimeCreatedDesc(creator);
    }

    @Override
    public List<Post> getAllByPrivacyOrderByTotalInteractionsDesc(PrivacyType privacyType) {
        return postRepository.getAllByPrivacyOrderByTotalInteractionsDesc(PrivacyType.PUBLIC);
    }


    @Override
    public List<Post> getPublicPosts() {
        return postRepository.getAllByPrivacyOrderByTimeCreatedDesc(PrivacyType.PUBLIC);
    }

    @Override
    public Post getPostById(int postId) {
        if (!postRepository.existsById(postId)) {
            throw new EntityNotFoundException(String.format(POST_NOT_EXIST, postId));
        }
        return postRepository.getPostById(postId);
    }

    @Override
    public List<Post> getCreatorPosts(int id, Principal principal) {
        if (!usersService.checkIfUserExists(id)) {
            throw new EntityNotFoundException(String.format(USER_NOT_EXIST, id));
        }

        UserInfo creator = usersService.getUserById(id);
        UserInfo loggedUser;

        if (principal == null) {
            return postRepository.getAllByPrivacyAndCreatedByOrderByTimeCreatedDesc(PrivacyType.PUBLIC, creator);
        } else {
            loggedUser = usersService.getByEmail(principal.getName());
        }

        if (friendsService.getBySenderIdAndReceiverId(creator.getId(), loggedUser.getId()) == null) {
            return postRepository.getAllByPrivacyAndCreatedByOrderByTimeCreatedDesc(PrivacyType.PUBLIC, creator);
        } else {
            return postRepository.getAllByCreatedByOrderByTimeCreatedDesc(creator);
        }
    }

    @Override
    public Post editPost(PostEditDTO postEditDTO) {
        Post postToEdit = postRepository.getPostById(postEditDTO.getId());

        postToEdit.setContent(postEditDTO.getContent());
        postToEdit.setTotalLikes(postEditDTO.getTotalLikes());

        return postRepository.save(postToEdit);
    }

    @Override
    public List<Post> getAll() {
        return postRepository.findAll();
    }

    public List<Post> getAllFriendsPosts(Principal principal) {
        UserInfo user = usersService.getByEmail(principal.getName());
        List<Friend> friends = friendsService.getAllBySenderId(user.getId());
        List<Post> temp = new ArrayList<>();
        for (Friend fr : friends) {
            temp.addAll(postRepository.getAllByCreatedByOrderByTimeCreatedDesc(fr.getReceiver()));
        }
        temp.addAll(getPublicPosts());
        temp.sort(Comparator.comparing(Post::getTimeCreated));
        return temp;
    }

    @PrePersist
    protected String onCreate() {
        LocalDateTime localDate = LocalDateTime.now();
        return DateTimeFormatter.ofPattern(TIME_FORMAT).format(localDate);
    }
}
