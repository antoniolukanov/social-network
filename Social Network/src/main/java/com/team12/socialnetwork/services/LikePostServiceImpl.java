package com.team12.socialnetwork.services;

import com.team12.socialnetwork.entities.DTOmodels.LikePostDTO;
import com.team12.socialnetwork.entities.LikePost;
import com.team12.socialnetwork.entities.Post;
import com.team12.socialnetwork.entities.UserInfo;
import com.team12.socialnetwork.exceptions.DuplicateEntityException;
import com.team12.socialnetwork.repositories.base.LikePostRepository;
import com.team12.socialnetwork.repositories.base.PostsRepository;
import com.team12.socialnetwork.services.base.LikePostService;
import com.team12.socialnetwork.services.base.PostsService;
import com.team12.socialnetwork.services.base.UsersService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.security.Principal;

@Service
@RequiredArgsConstructor
public class LikePostServiceImpl implements LikePostService {

    private final LikePostRepository likePostRepository;
    private final PostsRepository postsRepository;
    private final PostsService postsService;
    private final UsersService usersService;

    private static final String ALREADY_LIKED = "You have already liked that post!";

    @Override
    public void createLikePost(LikePost likePost) {
        likePostRepository.save(likePost);
    }

    @Override
    public void deleteLikePost(LikePost likePost) {
        likePostRepository.delete(likePost);
    }

    @Override
    public int likePost(LikePostDTO likePostDTO, Principal principal) {
        Post post = postsService.getPostById(likePostDTO.getPostId());
        UserInfo user = usersService.getByEmail(principal.getName());

        if (likePostRepository.existsByUserAndPost(user, post)) {
            throw new DuplicateEntityException(ALREADY_LIKED);
        }

        LikePost likePost = new LikePost(user, post);
        createLikePost(likePost);
        post.setTotalLikes(post.getTotalLikes() + 1);
        post.setTotalInteractions(post.getTotalInteractions() + 1);
        postsRepository.save(post);

        return post.getTotalLikes();
    }

    @Override
    public int dislikePost(LikePostDTO likePostDTO, Principal principal) {
        Post post = postsService.getPostById(likePostDTO.getPostId());
        UserInfo user = usersService.getByEmail(principal.getName());
        LikePost unlike = likePostRepository.getByUserAndPost(user, post);

        deleteLikePost(unlike);
        post.setTotalLikes(post.getTotalLikes() - 1);
        post.setTotalInteractions(post.getTotalInteractions() - 1);

        postsRepository.save(post);

        return post.getTotalLikes();
    }
}