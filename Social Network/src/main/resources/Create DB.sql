create schema social_network;

create table social_network.users
(
    username varchar(50) not null
        primary key,
    password varchar(68) not null,
    enabled  tinyint     not null
);

create table social_network.authorities
(
    username  varchar(50) not null,
    authority varchar(50) not null,
    constraint username_authority
        unique (username, authority),
    constraint authorities_fk
        foreign key (username) references users (username)
);

create table social_network.users_info
(
    id            int auto_increment
        primary key,
    first_name    varchar(50)                null,
    last_name     varchar(50)                null,
    email         varchar(50)                null,
    age           int default 0              null,
    address       varchar(100)               null,
    education     varchar(100)               null,
    privacy       enum ('PUBLIC', 'PRIVATE') null,
    cover_photo   longblob                   null,
    profile_photo longblob                   null,
    constraint users_info_users_username_fk
        foreign key (email) references users (username)
            on update cascade on delete cascade
);

create table social_network.friends
(
    id          int auto_increment
        primary key,
    sender_id   int null,
    receiver_id int null,
    constraint friends_users_info_user_id_fk
        foreign key (sender_id) references users_info (id)
            on update cascade on delete cascade,
    constraint friends_users_info_user_id_fk_2
        foreign key (receiver_id) references users_info (id)
            on update cascade on delete cascade
);

create table social_network.post_info
(
    id                 int auto_increment
        primary key,
    text_post          varchar(200)               null,
    picture_post       longblob                   null,
    privacy            enum ('PUBLIC', 'PRIVATE') null,
    time_created       timestamp                  null,
    created_by_id      int                        null,
    total_likes        int default 0              null,
    total_interactions int default 0              null,
    constraint post_info_users_info_id_fk
        foreign key (created_by_id) references users_info (id)
            on update cascade on delete cascade
);

create table social_network.comments
(
    id           int auto_increment
        primary key,
    comment      varchar(255)  null,
    created_by   int           null,
    for_post_id  int           null,
    total_likes  int default 0 null,
    time_created timestamp     null,
    constraint comments_post_info_post_id_fk
        foreign key (for_post_id) references post_info (id)
            on update cascade on delete cascade,
    constraint comments_users_info_user_id_fk
        foreign key (created_by) references users_info (id)
            on update cascade on delete cascade
);

create table social_network.comment_likes
(
    id         int auto_increment
        primary key,
    comment_id int null,
    user_id    int null,
    constraint comment_likes_comments_id_fk
        foreign key (comment_id) references comments (id),
    constraint comment_likes_users_info_id_fk
        foreign key (user_id) references users_info (id)
);

create index post_info_privacy_id_fk
    on post_info (privacy);

create table social_network.post_likes
(
    id      int auto_increment
        primary key,
    post_id int null,
    user_id int null,
    constraint post_likes_post_info_id_fk
        foreign key (post_id) references post_info (id)
            on update cascade on delete cascade,
    constraint post_likes_users_info_user_id_fk
        foreign key (user_id) references users_info (id)
);

create table social_network.requests
(
    id          int auto_increment
        primary key,
    sender_id   int null,
    receiver_id int null,
    constraint requests_users_info_user_id_fk
        foreign key (sender_id) references users_info (id)
            on update cascade on delete cascade,
    constraint requests_users_info_user_id_fk_2
        foreign key (receiver_id) references users_info (id)
            on update cascade on delete cascade
);

