"use strict";
$(function () {
    registerUser();
});

function registerUser() {
    $(".registration-form-one").on("click", "#register", function () {

        let $firstName = $("#first-name").val();
        let $lastName = $("#last-name").val();
        let $password = $("#password-reg").val();
        let $confirmPassword = $("#password-reg-conf").val();
        let $email = $("#email-reg").val();


        let requestBody = {
            "email": $email,
            "password": $password,
            "confirmPassword": $confirmPassword,
            "firstName": $firstName,
            "lastName": $lastName,

        };

        fetch("/api/users/", {
            method: "POST",
            body: JSON.stringify(requestBody),
            headers: {
                "Content-type": "application/json",
                "Accept": "application/json, text/plain, */*"
            }
        })
            .then((response) => {
                if (response.status !== 200) {
                    toastr.error('Please provide valid input!');
                } else {
                    toastr.info('Successful registration!');
                    window.location.href = '/login'
                }
            });
    });
}