"use strict";
$(function () {
    getAndLoadContent();
});

function getAndLoadContent() {

    $.ajax({
        url: "/api/posts/public/" ,
        type: "GET",
        success: function (response) {
            let $posts = $("#posts");
            response.forEach(function (post) {
                $posts.append(
                    " <div id='post-" + post.id + "' class=\"central-meta item postIdForLike\">" +
                    "<div class='user-post'>" +
                    " <div class='friend-info'>" +
                    "  <figure>" +
                    "    <img id='image' src=' " + "data:image/png;base64," + post.createdBy.profilePicture + " ' alt=''>" +
                    "  </figure>" +
                    " <div class='friend-name'>" +
                    "   <ins><a href='/profile/" + post.createdBy.id + "' title=''>" + post.createdBy.firstName + ' ' + post.createdBy.lastName + "</a></ins>" +
                    "    <span>" + post.timeCreated + "</span>" +
                    " </div>" +
                    "<div class='post-meta'>" +
                    "<img src='" + "data:image/png;base64," + post.picturePost + " ' alt=''>" +
                    "<div class='description'>" +
                    "<p  id='commentcont' >" + post.content + "</p>" +
                    "  </div>" +
                    "<div class='we-video-info'>" +
                    " <ul>" +
                    "  <li>" +
                    "   <span class='comment' id='comment' data-toggle='tooltip'  title='Comments'>" +
                    "    <i class='fa fa-comments-o '></i>" +
                    "     <ins class='total-comments' id='total-comments'>" + post.comments.length + "</ins>" +
                    "   </span>" +
                    "  </li>" +
                    "  <li>" +
                    "   <span class='like' data-toggle='tooltip' title='like'>" +
                    "    <i class='ti-heart' id='" + post.id + "'></i>" +
                    "      <ins class='totalLikes '>" + post.totalLikes + "</ins>" +
                    "   </span>" +
                    "  </li>" +
                    " </ul>" +
                    "</div>" +
                    "</div>" +
                    "  <div class='coment-area' id='comments'>" +
                    "  <ul class='we-comet'>" +
                    "</ul>" +
                    "   <li>" +
                    "  <a id='expand-" + post.id + "' class='showmore underline'> Show more comments </a>" +
                    "<div id='show-" + post.id + "' class='comment-div showHide' style='display:none'> " +
                    "</div>" +
                    " </li>" +
                    "</div>" +
                    "</div>" +
                    "</div>"
                );

                let postIdApp = "post-" + post.id;
                let array = post.comments;
                let i;
                for (i = 0; i < post.comments.length; i++) {
                    if (i < post.comments.length - 2) {
                        $("#posts").find("#" + postIdApp + " .showHide").prepend(
                            "<li>" +
                            "<div class='comet-avatar'>" +
                            "<img src=' " + "data:image/png;base64," + array[i].creator.profilePicture + " '  alt=''>" +
                            "</div>" +
                            "<div class='commentId commentIdForLike' id='com-id-" + array[i].id + "'>" +
                            "<div class='coment-head'>" +
                            "<h5>" + array[i].creator.firstName + ' ' + array[i].creator.lastName + " </h5>" +
                            "<span>" + array[i].timeCreated + "</span>" +
                            "<span class='comment' id='comment' data-toggle='tooltip'  title='Comments'>" +
                            "    <i class='ti-heart '></i>" +
                            "     <ins class='comments-likes' id='comments-likes'>" + array[i].totalLikes + "</ins>" +
                            "   </span>" +
                            "</div>" +
                            "<p>" + array[i].content + "</p>" +
                            "</div>" +
                            "</li>")
                    } else {
                        $("#posts").find("#" + postIdApp + " .we-comet").prepend(
                            "<li>" +
                            "<div class='comet-avatar'>" +
                            "<img src=' " + "data:image/png;base64," + array[i].creator.profilePicture + " '  alt=''>" +
                            "</div>" +
                            "<div class='commentId commentIdForLike' id='com-id-" + array[i].id + "'>" +
                            "<div class='coment-head'>" +
                            "<h5>" + array[i].creator.firstName + ' ' + array[i].creator.lastName + " </h5>" +
                            "<span>" + array[i].timeCreated + "</span>" +
                            "<span class='comment' id='comment' data-toggle='tooltip'  title='Comments'>" +
                            "    <i class='ti-heart  '></i>" +
                            "     <ins class='comments-likes' id='comments-likes'>" + array[i].totalLikes + "</ins>" +
                            "   </span>" +
                            "</div>" +
                            "<p>" + array[i].content + "</p>" +
                            "</div>" +
                            "</li>"
                        );
                    }
                }
            });
        },
        error: function () {
            toastr.error('Something went wrong.');
        }
    })
}
