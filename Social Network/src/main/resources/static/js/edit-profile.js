"use strict";
$(function () {
    validateInput();
});

function validateInput() {
    var validator = $(".editing-info").validate({
        rules: {
            firstName: "required",
            lastName: "required",
            age: "required",
            address: "required",
            education: "required"
        }
    });
    if (validator.form()) {
        alert('Success');
    }
}