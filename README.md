# Social Network - Team 12

This is our final project from Telerik Academy with parther EGT Interactive.
We are Team 12: Antonio Lukanov and Viktor Petrov.
Link to our trello board : [https://trello.com/b/2cMhFZuy/social-network](https://trello.com/b/2cMhFZuy/social-network)

This application is writen in JDK 8.
This application is a spring web project, based on social network. You can create post(text or with picture), comment posts, 
like and dislike posts, like and dislike comments, able to connect to other profiles via friend request, browse your own friends.
To be able to run this application you need to create database(source file for create is in resources).
Before you register new user you need to set the absolute path for profile picture and cover photo in UsersServiceImpl.class.
We used MariaDB to store our data.